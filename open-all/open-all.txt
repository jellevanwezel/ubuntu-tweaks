# open function, open anything
# put this on the bottom of your ~/.bashrc file 
function open() {
	xdg-open "$@" 2> /dev/null
}
